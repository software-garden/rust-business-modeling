use std::{fmt::Display, io, vec};

/* TYPES */

pub struct Calendar {
    events: Vec<Event>,
}

pub struct Event {
    pub start: Time,
    pub end: Time,
    pub description: String,
}

pub enum Time {
    Date {
        year: i16,
        month: u8,
        day: u8,
    },
    DateTime {
        year: i16,
        month: u8,
        day: u8,
        hour: u8,
        minute: u8,
    },
}

/* IMPLEMENTATION */

fn main() -> io::Result<()> {
    // Generate a calendar
    let mut calendar: Calendar = Calendar::default();
    calendar.schedule(Event {
        description: "The Big Day 📅".to_string(),
        start: Time::Date { year: 2022, month: 05, day: 24, },
        end: Time::Date { year: 2022, month: 05, day: 24, },
    });
    calendar.schedule(Event {
        description: "Amsterdam Rust meetup 🦀".to_string(),
        start: Time::DateTime { year: 2022, month: 05, day: 24, hour: 18, minute: 30, },
        end: Time::DateTime { year: 2022, month: 05, day: 24, hour: 21, minute: 00, },
    });
    calendar.schedule(Event {
        description: "Watch The Simpsons 📺".to_string(),
        start: Time::DateTime { year: 2022, month: 05, day: 25, hour: 13, minute: 30, },
        end: Time::Date { year: 2022, month: 05, day: 27, },
    });

    // Output
    println!("{}", calendar);

    // It's all good man!
    Ok(())
}

impl Calendar {
    fn new() -> Self {
        Calendar { events: vec![] }
    }
    fn schedule(&mut self, event: Event) -> &Self {
        self.events.push(event);
        self
    }
}

impl Default for Calendar {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for Calendar {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "There are {} events scheduled", self.events.len())?;
        writeln!(f, "")?;
        for event in &self.events {
            writeln!(f, "{}", event)?;
        }
        Ok(())
    }
}

impl Display for Event {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match (&self.start, &self.end) {
            (Time::Date { .. }, Time::Date { .. }) => {
                writeln!(
                    f,
                    "A whole day event from {} until {}:",
                    self.start, self.end
                )?;
            }

            (Time::DateTime { .. }, Time::DateTime { .. }) => {
                writeln!(f, "A regular event from {} until {}:", self.start, self.end)?;
            }
        }
        writeln!(f, "{}", self.description)?;
        writeln!(f, "")
    }
}

impl Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Time::Date { year, month, day } => {
                write!(f, "{}-{:02}-{:02}", year, month, day)
            }

            Time::DateTime {
                year,
                month,
                day,
                hour,
                minute,
            } => {
                write!(
                    f,
                    "{}-{:02}-{:02} {:02}:{:02}",
                    year, month, day, hour, minute
                )
            }
        }
    }
}

