Before the talk:

1. Open the slides.
2. Open the speaker notes.
3. Arrange windows on displays.
4. Smile 😁


# Clarity for the Nerd

Hello and good evening! What a wonderful crowd we have here!

My name is Tad Lispy and I am a nerd and I want to talk to you about nerdy things.

Recently I've been thinking a lot about how software is developed. It seems to me that it is best developed in collaboration with other people. Before we start I'd like to learn a bit about you - the audience. I'd like to invite you to a quick show of hands.

How many of you are software developers? Developers, could you please rise your hands?

Keep it up please. And who wants to be a software developer. It's OK to rise to have both hands up.

That's awesome. Thank you. And who is a designer? Or a product manager?

Do we have any entrepreneurs? Startup founders?

Are there any teachers among us?

Finally, is there anyone who have not raised a hand yet and would like to introduce themselves?

Thank you.

Today I have a privilege to talk to you about the choices and combinations. It is a useful tool for thinking that can help you understand the business context in which your software operates. My talk is tailored for developers, but hopefully it will be useful for everyone in here. I will show you some Rust code, but please bear in mind that it's just for illustration. My point is not so much about Rust or even programming, but about patterns of thinking.

So let's consider the collaborative nature of software development. It involves various people. It should always start and end with the users. There are also managers, designers, systems operators and of course the developers. In this context I believe a developers are like translators - they translate the knowledge about the business into working, useful software. A good translator should have a deep understanding of the subject they deal with. I believe the same applies to a developer and a lot of work we do at Software Garden is to help developers become business domain experts.

Let me explain what I mean when I say business domain expert. What is a business domain?

For the purpose of this talk a business is a group of people doing something in an organized way. It doesn't have to be formal, commercial or for-profit. It can be a company, but also a gevernment agency, a non-profit or a free-software project built on-line. Whenever people are busy doing something together I call it a business. To be effective those people need to understand each other. They need to know what they are doing, why and how. This knowledge specific to a particular business is called a "business domain".

So a business domain is the knowledge about the business - a knowledge in the heads of people. I believe this knowledge should be transparent and shared among all the people involved or affected by the business activity. In a healthy organization this knowledge constantly evolves through a meaningful, empowering debate. There is a collective responsibility to enable the communication, but also a personal responsibility to understand and be able to express thoughts about the business. That means that you, as a developer, need to understand your business domain and be comfortable talking about it.

Today I'm going to present only one of many tools to think about a business domain. I call it "choices and combinations". In Software Garden we find it particularly useful to think about businesses of our clients in terms of choices and combinations. It is a very powerful tool, applicable to a variety of real life situations, and It translates well into code. It's especially helpful to recognize and deal with invalid states. An invalid state is a situation that doesn't make sense in the context of a given domain.

To demonstrate it with an example, let's suppose we are modeling a calendar. In the calendar there are events. For simplicity let's say events are like this:

- On Tuesday, from 18:30 to 21:00 I'll be spending time with the wonderful Rust crowd.

or

- From Friday until Wednesday I'll be on Holiday.

We can see that:

1. Each event has a start 
2. Each event has an end 
3. Start and end can be either a day (for example holidays) or a date and time (for example meetups)

Obviously there is more to events, but it's just an example, so let's keep it simple. Here's one way to model it in Rust.

> Show the code in `main` branch
> Use LSP to fill missing branches

We see that there are some `todo!` macros left for us to fill. But how can we fill them? What kind of an event has a start date and an end time? Does it even make sense?

If we think about it, an event is either a **whole day event** with start date and end date (like an anniversary, or a day off), or **regular event** like an appointment or a meeting, with a start time and an end time. We see that holidays and anniversaries are just a different variant of an event than meetings and appointments. A whole day event and a regular event has a different, incompatible structure. So every time we create an event we have to make a choice - regular or whole day.

> Show the other branch with item `enum`.

Once we implement the choice pattern for events it is no longer possible to mix dates and times. The domain is properly expressed and we are no longer able to represent invalid states in our code. In terms of choices and combinations we moved from the previous model, where:

    Event is a combination of start, end and description

    Start and end is a choice between date or time.

To a new model where:

    A calendar item is a choice between a regular event or a whole day event.

    A whole day event is a combination of a start date, an end date and a description.

    A regular event is a combination of a start time, an end time, a timezone and a description.

Obviously this is a very simple example, but hopefully it illustrates the principle of modeling real life domains with choices and combinations. In Rust we model choices as `enums`, and combinations with `structs`. More mathematically oriented people may have noticed, that the choices correspond to sum types and combinations to product types. Indeed this is just one of the tools offered by the discipline of **type driven design**.

Using types to model business domain provides a lot of benefits:

- The compiler helps us expose contradictions in the way we understand the business. It becomes a tool for thinking.
- Developers gain clarity about the domain.
- Invalid states cannot be represented in code and we don't have to worry about them.

It's time to wrap it up. Today I presented choices  and combinations. The choices are also known as sum types and can be modeled with `enums`. Combinations are product types and we can use `structs` to model them. We saw how they can be used to reason and communicate about the business domains.

If you find this kind of thing interesting, please get in touch with me. At Software Garden we offer paid as well as free workshops for developers, designers, product and business people. We are helping the participants arrive at shared understanding and collaborate better.

If we still have time I'll be happy to take questions or hear your polite thoughts.
