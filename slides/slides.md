---
title: Clarity <br/><small>for the Nerd</small>
author: |
  Tad Lispy  
  <img src="./software-garden-logotype.svg" title="Software Garden" />
title-slide-attributes:
  data-background: hsl(358, 51%, 37%)
date: 2022-05-24
css: style.css
---

# Who? How? { background-image="fallon-michael-raised-hands-unsplash.jpg" }

How is software developed
and who are you?

<aside class="notes">
Recently I've been thinkig a lot...

- Developers?
- Wanabees?
- Designers? Product managers?
- Entrepreneurs? Founders?
- Teachers?
- Anyone else?
</aside>


# About what?

Choices and combinations is a tool for thinking 🧠

Code is just for illustration 👩‍💻


<aside class="notes">
Today I'm going to talk to you about...

Talk is tailored for developers
</aside>


# Collaboration { background-image="learn-think-communicate.png" }

Be a domain expert.

Your job is to learn, think and communicate.


<aside class="notes">
Users, managers, designers, operators and of course the developers.

Developers as translators.

Translators need to be domain experts.

At Software Garden we help developers become domain experts
</aside>


# The Business Domain { background-image="monty-python-the-business-domain.jpg" }


<aside class="notes">
A group of people doing something in an organized way.

Need to understand each other, what they are doing, why and how.

Business domain is the knowledge in the heads of people.

Transparent and shared.

Constantly evolves through a meaningful, empowering debate

Every persons responsibility to understand and  express thoughts.
</aside>


# Choices and Combinations { background-image="kenny-eliason-tools-unsplash.jpg" }

Translates well into code

Recognize invalid states


<aside class="notes">
One among many tools to think about a business domain.

In Software Garden we find it particularly useful.

Powerful and applicable to a variety of real life situations.

An invalid state is a situation that doesn't make sense in the context of a given domain.
</aside>


# Example

Calendar events:

- Tuesday, 18:30 - 21:00

  Have fun with the wonderful crowd of Rustaceans 🦀
  
- Wednesday - Friday

  Watch The Simpsons non-stop


<aside class="notes">
1. Each event has a start 
2. Each event has an end 
3. Start and end can be either a day (for example holidays) or a date and time (for example meetups)
</aside>


# Code time!

For simplicity we will gloss over a lot of details


## A Calendar

::: row
``` rust
pub struct Calendar {
    events: Vec<Event>,
}

pub struct Event {
    pub start: Time,
    pub end: Time,
    pub description: String,
}
```

``` rust
pub enum Time {
    Date {
        year: i16,
        month: u8,
        day: u8,
    },
    DateTime {
        year: i16,
        month: u8,
        day: u8,
        hour: u8,
        minute: u8,
    },
}
```
:::

## Adding events

::: carousel
:::: { .fragment }
``` rust
calendar.schedule(Event {
    description: "The Big Day 📅".to_string(),
    start: Time::Date { year: 2022, month: 05, day: 24, },
    end: Time::Date { year: 2022, month: 05, day: 24, },
});
```
::::

:::: { .fragment }
``` rust
calendar.schedule(Event {
    description: "Amsterdam Rust meetup 🦀".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 18, minute: 30,
    },
    end: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 21, minute: 00,
    },
});
```
::::

:::: { .fragment }
``` rust
calendar.schedule(Event {
    description: "Watch The Simpsons 📺".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 25,
        hour: 13, minute: 30,
    },
    end: Time::Date { year: 2022, month: 05, day: 27, },
});
```
::::
:::


## Does it work?

![Non-exhaustive patterns](./non-exhaustive-patterns-poster.svg)


## What's wrong

::: { .carousel style="align-items: start" }
:::: { .fragment }
``` rust
match (&self.start, &self.end) {
  (Time::Date { .. }, Time::Date { .. }) => writeln!(
    f, "A whole day event from {} until {}:",
    self.start, self.end)?,


  (Time::DateTime { .. }, Time::DateTime { .. }) => writeln!(
    f, "A regular event from {} until {}:",
    self.start, self.end
  )?,
}
```
::::

:::: fragment
``` rust
match (&self.start, &self.end) {
  (Time::Date { .. }, Time::Date { .. }) => writeln!(
    f, "A whole day event from {} until {}:",
    self.start, self.end)?,


  (Time::DateTime { .. }, Time::DateTime { .. }) => writeln!(
    f, "A regular event from {} until {}:",
    self.start, self.end
  )?,

  (Time::Date { .. }, Time::DateTime { .. }) => todo!(),
  (Time::DateTime { .. }, Time::Date { .. }) => todo!(),
}
```
::::
:::
  </code>
</pre>

## How do we fix it? { background-image="inside-out.gif" }

::: { .row .fragment }
``` rust
pub struct Calendar {
    events: Vec<CalendarItem>,
}

pub enum CalendarItem {
    RegularEvent {
        start: DateTime,
        end: DateTime,
        description: String,
    },
    WholeDayEvent {
        start: Date,
        end: Date,
        description: String,
    },
}
```

``` rust
pub struct Date {
    year: i16,
    month: u8,
    day: u8,
}

pub struct DateTime {
    year: i16,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
}
```
:::


## Adding calendar items

::: carousel
:::: fragment
```rust
calendar.schedule(CalendarItem::WholeDayEvent  {
  description: "The Big Day 📅".to_string(),
  start: Date { year: 2022, month: 05, day: 24, },
  end: Date { year: 2022, month: 05, day: 24, },
});
```
::::
:::: fragment
```rust
calendar.schedule(CalendarItem::RegularEvent  {
  description: "Amsterdam Rust meetup 🦀".to_string(),
  start: DateTime { year: 2022, month: 05, day: 24, hour: 18, minute: 30, },
  end: DateTime { year: 2022, month: 05, day: 24, hour: 21, minute: 00, },
});
```
::::
:::: fragment
```rust
calendar.schedule(CalendarItem::WholeDayEvent  {
  description: "Watch The Simpsons 📺".to_string(),
  start: Date { year: 2022, month: 05, day: 25, },
  end: Date { year: 2022, month: 05, day: 27, },
});
```
::::
:::


## Better?

```rust
match self {
  CalendarItem::WholeDayEvent { start, end, description, } => {
      writeln!(f, "A whole day event from {} until {}:", start, end)?;
      writeln!(f, "{}", description)?;
  }

  CalendarItem::RegularEvent { start, end, description, } => {
      writeln!(f, "A regular event from {} until {}:", start, end)?;
      writeln!(f, "{}", description)?;
  }
}
```


## Does it work now?

![](./working-program-poster.svg){ .fragment }

# Before and After 

```
Event = Time and Time and Description
Time = Day or DateTime
```

⇓

```
CalendarItem = RegularEvent or WholeDayEvent
RegularEvent = DateTime and DateTime and Description
WholeDayEvent = Day and Day and Description
```

<aside class="notes">
It is no longer possible to mix dates and times

The domain is properly expressed in code.

We are no longer able to represent invalid states in our code
</aside>


# Type Driven Design { background-image="the-thinker-statue.png" }

Use your compiler to think.

Gain clarity about the domain.

Make invalid states unrepresentable.


<aside class="notes">
Simple, but illustrates modeling of real life domains with choices and combinations.

Choices - `enums`

Combinations - `structs`

Sum and product types 🧑🏿‍🔬
</aside>

# What again?

- Choices - Sum Types - Enums
- Combinations - Product Types - Structs
- Think clearly about your domain


<aside class="notes">
It's time to wrap it up. 

Today I presented choices and combinations as a framework to think about a business domain.
</aside>


# Thank you

::: row
![](./tad-lispy.png){ .logo }

![](./software-garden-logotype.svg){ .logo }
:::

<small>
<https://software-garden.gitlab.io/rust-business-modeling/>
</small>

<aside class="notes">
If you find this kind of thing interesting ...

Free and paid workshops for developers, designers, product and business people.

Shared understanding. Collaborate better.

Questions? Polite thoughts?
</aside>
