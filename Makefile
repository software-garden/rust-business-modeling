include Makefile.d/defaults.mk

all: ## Build the website (DEFAULT)
all: www
.PHONY: all

test: ## Run tests for the sample code
test:
	cargo test
.PHONY: test

markdown := $(shell find slides/ -iname '*.md')
css-source := $(shell find slides/ -iname '*.css')
images-source := $(shell find slides/ \
	-iname '*.png' -or \
	-iname '*.gif' -or \
	-iname '*.jpeg' -or  \
	-iname '*.jpg'  -or \
	-iname '*.svg' \
)
videos-source := $(shell find slides/ -iname '*.webm')

html := $(patsubst slides/%.md,www/%.html,$(markdown))
css := $(patsubst slides/%.css,www/%.css,$(css-source))
images := $(patsubst slides/%,www/%,$(images-source))
videos := $(patsubst slides/%.webm,www/%.webm,$(videos-source))

www: ## Compile slides into the www/ directory
www: $(html)
www: $(css)
www: $(images)
www: $(videos)
www: www/index.html
www:
	touch $@

install: ## Install the program in the ${prefix} directory
install: prefix ?= $(out)
install: www
	test $(prefix)
	mkdir --parents $(prefix)
	cp --recursive www/* $(prefix)
.PHONY: install

www/index.html: essay.md slides/essay-meta.html
www/index.html:
	mkdir --parents $(@D)
	pandoc \
		--standalone \
		--include-in-header=slides/essay-meta.html \
		--to=html \
		--output=$@ \
		$<

www/%.html: slides/%.md slides/style.css
	mkdir --parents $(@D)
	pandoc \
		--standalone \
		--resource-path=slides/ \
		--highlight-style=zenburn \
		--to=revealjs \
		--slide-level=2 \
		--output=$@ \
		$<

www/%.css: slides/%.css
	mkdir --parents $(@D)
	cp $< $@

www/%.png: slides/%.png
	mkdir --parents $(@D)
	cp $< $@

www/%.jpeg: slides/%.jpeg
	mkdir --parents $(@D)
	cp $< $@

www/%.jpg: slides/%.jpg
	mkdir --parents $(@D)
	cp $< $@

www/%.gif: slides/%.gif
	mkdir --parents $(@D)
	cp $< $@

www/%.svg: slides/%.svg
	mkdir --parents $(@D)
	cp $< $@

www/%.webm: slides/%.webm
	mkdir --parents $(@D)
	cp $< $@

### DEVELOPMENT

develop: ## Watch, rebuild and serve the content
develop:
	watch --interval=0.1 --errexit make www
.PHONY: develop

serve: ## Serve the built program
serve: www
serve:
	miniserve --index=index.html www/
.PHONY: serve

clean: ## Remove all files set to be ignored by git
clean:
	git clean -dfX
.PHONY: clean

### NIX SPECIFIC

export nix := nix --experimental-features "nix-command flakes"
export nix-cache-name := software-garden

result: ## Build the program using Nix
result:
	cachix use $(nix-cache-name)
	$(nix) build --print-build-logs

nix-cache: ## Push Nix binary cache to Cachix
nix-cache: result
	$(nix) flake archive --json \
	| jq --raw-output '.path, (.inputs | to_entries [] .value.path)' \
	| cachix push $(nix-cache-name)

	$(nix) path-info --recursive \
	| cachix push $(nix-cache-name)

### HELP

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help
