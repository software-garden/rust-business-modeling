---
title: Clarity <br/><small>for the Nerd</small>
author: Tad Lispy, Software Garden
date: 2022-05-24
---

In this essay I present _choices and combinations_ as a framework to think and communicating about a _business domain_ and to translate it into code using the Rust programming language. It is an expanded version of the talk I'm going to deliver at [Rust Developers Amsterdam meetup on Tuesday, 2022-05-24 at 18:30](https://www.meetup.com/rust-amsterdam-group/events/285291653/).

👨‍🏫 **[the slides](slides.html)**

# The Collaborative Nature of Software Development

![](learn-think-communicate.png)

Let's consider the collaborative nature of software development. It involves various people - users, managers, designers, operators and of course the developers. In this context I believe developers are like translators. They translate the knowledge about the business into working, useful software. A good translator must be a domain expert, with deep understanding of the subject they deal with. The same applies to a developer. A lot of work we do at Software Garden is to help developers become business domain experts.

The following is tailored for developers, but hopefully it will be useful for other software professionals too. It is about a useful thinking tool I call _choices and combinations_. The framework can help you understand the business context in which your software operates. To illustrate my arguments I will present some very simplistic Rust code, but my point is not so much about programming, but about patterns of thinking and organization of work.


# The Business Domain

![](monty-python-the-business-domain.jpg)

For the purpose of this talk a _business_ is a group of people doing something in an organized way. It doesn't have to be formal, commercial or for-profit. From this perspective a government agency, non-profit or free software project is a business. For the business to be effective, people involved in it need to understand each other: what they are doing, why and how. This understanding is called a _business domain_.

So a business domain is the knowledge about the business. This knowledge is in the heads of people and I believe it should be transparent and shared among anyone involved or affected by the business activity. In a healthy organization this knowledge constantly evolves through a meaningful, empowering debate. There is a collective responsibility to enable the communication, but also a personal responsibility to understand and be able to express thoughts about the business. In other words you, as a developer, need to understand your business domain and be comfortable discussing it.

There are many tools that can help us think about a business domain. Here I want to focus on one of them - the framework of _choices and combinations_.


# Choices and Combination

![](kenny-eliason-tools-unsplash.jpg)

In Software Garden we find it particularly useful to think about businesses in these terms. It's a very powerful tool, applicable to a variety of real life situations. It is a framework that translates well into code and it's especially helpful to recognize and deal with invalid states. An _invalid state_ is a situation that doesn't make sense in the context of a given domain.

I'll demonstrate it with a simple example. Let's suppose we are modeling a calendar. In the calendar there are events. For simplicity let's say events are like this:

> On Tuesday, from 18:30 to 21:00 I'll be spending time with the wonderful Rust crowd.

or

> From Friday until Wednesday I'll be on Holiday.

From these samples we can infer that:

1. Each event has a start 
2. Each event has an end 
3. Start and end can be either a day (for example holidays) or a date and time (for example meetups)

Obviously there is more to events and calendars. It's just an example, so let's gloss over details and keep it simple. Here's one way to model it in Rust.


## The code v. 1

Let's start with the definition of types. 

``` { .rust .numberLines }
pub struct Calendar {
    events: Vec<Event>,
}

pub struct Event {
    pub start: Time,
    pub end: Time,
    pub description: String,
}

pub enum Time {
    Date {
        year: i16,
        month: u8,
        day: u8,
    },
    DateTime {
        year: i16,
        month: u8,
        day: u8,
        hour: u8,
        minute: u8,
    },
}
```

We can recognize the choices and combinations above. An event is a combination of a start time, an end time and a description. A time is a choice between a date or date-time. A date is again combinations of a year, a month and a day. Similar for date-time. We will get to this concepts again later.

Here is how events can be scheduled in a calendar:

``` { .rust .numberLines }
let mut calendar: Calendar = Calendar::default();

calendar.schedule(Event {
    description: "The Big Day 📅".to_string(),
    start: Time::Date { year: 2022, month: 05, day: 24, },
    end: Time::Date { year: 2022, month: 05, day: 24, },
});

calendar.schedule(Event {
    description: "Amsterdam Rust meetup 🦀".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 18, minute: 30,
    },
    end: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 21, minute: 00,
    },
});

calendar.schedule(Event {
    description: "Watch The Simpsons 📺".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 25,
        hour: 13, minute: 30,
    },
    end: Time::Date { year: 2022, month: 05, day: 27, },
});
```

But if we try to run it - it doesn't work!

![](./non-exhaustive-patterns-poster.svg)

The message is cryptic. If we look closely we can discern that it's something about two patterns not being covered.

```
patterns `(&Date { .. }, &DateTime { .. })` and `(&DateTime { .. }, &Date { .. })` not covered
```

The compiler also tells us that the offending code is in the `main.rs` file on line 87. Let's look.

``` { .rust .numberLines startFrom=85 }
impl Display for Event {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    match (&self.start, &self.end) {
      (Time::Date { .. }, Time::Date { .. }) => writeln!(
        f, "A whole day event from {} until {}:",
        self.start, self.end
      )?,
    
      (Time::DateTime { .. }, Time::DateTime { .. }) => writeln!(
        f, "A regular event from {} until {}:",
        self.start, self.end
      )?,
    }
    writeln!(f, "{}", self.description)?;
    writeln!(f, "")
  }
}
```

It's the display logic for an event, responsible for representing it as text. The implementation is also quite hard to follow. Let's ask the computer to add the missing patterns and zoom in on the `match` expression.


``` { .rust .numberLines startFrom=87 }
match (&self.start, &self.end) {
  (Time::Date { .. }, Time::Date { .. }) => writeln!(
    f, "A whole day event from {} until {}:",
    self.start, self.end
  )?,


  (Time::DateTime { .. }, Time::DateTime { .. }) => writeln!(
    f, "A regular event from {} until {}:",
    self.start, self.end
  )?,

  (Time::Date { .. }, Time::DateTime { .. }) => todo!(),
  (Time::DateTime { .. }, Time::Date { .. }) => todo!(),
}
```

Two new patterns were inserted, but there are some `todo!` macros left for us to fill. The computer was able to detect that some possible situations are not accounted for and even write the structure to cover those situations. But filling this structure by making decisions what to do with those situations is a job for a developer. How should we fill it? What kind of an event has a start date and an end time? Does it even make sense?

If we think about it, an event is either

- a _whole day event_

  like an anniversary, or a day off with a start date and an end date; or
  
- a _regular event_ 

  like an appointment or a meeting, with a start time and an end time.
  
We see that holidays and meetings are just different variants of events. A whole day event has a different structure that is incompatible with a regular event. Every time an event is created we have to make a choice.


## Choices and Combinations Rearranged

![](christina-wocintechchat-refactoring-unsplash.jpg)

Here's how this new thinking about events can be modeled:

``` { .rust .numberLines }
pub struct Calendar {
    events: Vec<CalendarItem>,
}

pub enum CalendarItem {
    RegularEvent {
        start: DateTime,
        end: DateTime,
        description: String,
    },
    WholeDayEvent {
        start: Date,
        end: Date,
        description: String,
    },
}

pub struct Date {
    year: i16,
    month: u8,
    day: u8,
}

pub struct DateTime {
    year: i16,
    month: u8,
    day: u8,
    hour: u8,
    minute: u8,
}
```

A _whole day event_ and a _regular event_ are two variants of a _calendar item_. There is no abstract concept of _time_ anymore, because we realized that date and date-time are separate things, that in our domain cannot be mixed. With this types it is no longer possible to incorrectly mix dates and times.

``` { .rust .numberLines }
calendar.schedule(Event {
    description: "The Big Day 📅".to_string(),
    start: Time::Date { year: 2022, month: 05, day: 24, },
    end: Time::Date { year: 2022, month: 05, day: 24, },
});

calendar.schedule(Event {
    description: "Amsterdam Rust meetup 🦀".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 18, minute: 30,
    },
    end: Time::DateTime {
        year: 2022, month: 05, day: 24,
        hour: 21, minute: 00,
    },
});

calendar.schedule(Event {
    description: "Watch The Simpsons 📺".to_string(),
    start: Time::DateTime {
        year: 2022, month: 05, day: 25,
        hour: 13, minute: 30,
    },
    end: Time::Date { year: 2022, month: 05, day: 27, },
});
```

The domain is properly expressed. It is no longer possible to represent invalid states in our code. In terms of _choices and combinations_ we moved from the previous model, where:

  - An event is a _combination_ of a start, an end and a description
  
  - A start and end is a _choice_ between a date or a time.

To a new model where:

  - A calendar item is a _choice_ between a regular event or a whole day event.
  
  - A whole day event is a _combination_ of a start date, an end date and a description.
  
  - A regular event is a _combination_ of a start time, an end time and a description.

Perhaps the most obvious gain is in the display logic, where previously we had cryptic code with cryptic errors, now we have this:

``` { .rust .numberLines startFrom=87 }
match self {
  CalendarItem::WholeDayEvent { start, end, description, } => {
      writeln!(f, "A whole day event from {} until {}:", start, end)?;
      writeln!(f, "{}", description)?;
  }

  CalendarItem::RegularEvent { start, end, description, } => {
      writeln!(f, "A regular event from {} until {}:", start, end)?;
      writeln!(f, "{}", description)?;
  }
}
```

Obviously this is a very simple example, but hopefully it illustrates the principle of modeling real life domains with choices and combinations. In Rust we model choices as `enums`, and combinations with `structs`. More mathematically oriented people may have noticed, that the choices correspond to sum types and combinations to product types. Indeed this is just one of the many tools offered by the discipline of type driven design.


# Type Driven Design

![](the-thinker-statue.png)

Using types to model a business domain provides a lot of benefits:

  - Code represents a shared mental model

    Type driven design helps to implement the ubiquitous language concept from the discipline of domain driven design. We can use types to directly represent concepts from the business domain.


  - Developers gain clarity about the domain

    Using types to model business rules can help you understand the domain at a deeper level. This is sometimes called refactoring towards deeper understanding. If we go back to the metaphor of a developer as a translator, then it becomes obvious that they cannot do their job well without understanding the domain. Sometimes it's called GIGO (garbage in - garbage out). Design is the process of reducing the amount of garbage coming into our brains, so that we can have better products faster.


  - The compiler becomes a tool for thinking

    It can help us expose contradictions in the way we understand and model the business domain. Tools that help verify the soundness of a model bring value not only for developers but the organization as a whole.


  - The code becomes less complicated

    When invalid states cannot be represented in code we don't have to worry about them anymore. This usually translates into less defensive or convoluted code - a well known breeding environment for bugs.

    On the flip-side the types usually become more complex. But that's because the types encode a complex business domain. If the types are less complicated than the domain, then they are incorrect. And trying to compensate for it with runtime checks just moves the complexity somewhere else and usually scatters and magnifies it, at the same time denying us the compile-time checks.


  - Less documentation to maintain

    Documentation has a nasty habit of getting outdated. Maintaining it is a hard and thankless job so it's often neglected. Outdated documentation can cause more harm than good.  Types can become a living documentation of the domain, and tools like compilers can ensure it stays fresh.


# Summary and References

In this essay I presented choices and combinations as framework for thinking and communicating about a business domain. The choices are also known as sum types and in the Rust programming language can be modeled with `enums`. Combinations are product types and we can use `structs` to model them. Above all I argued that the job of a developer is to learn, think and communicate. When working like this, writing and maintaining code becomes relatively easy.

If you find this kind of thing interesting, please get in touch with me. At [Software Garden](https://software.garden/) we offer paid as well as free workshops for developers, designers, product and business people. We are helping the participants arrive at shared understanding and collaborate better. We are all about happiness and efficiency at work.


---

The thoughts presented here are inspired primarily by [the Domain Modeling Made Functional](https://www.youtube.com/watch?v=2JB1_e5wZmU), a talk by [Scott Wlaschin](https://scottwlaschin.com/).
