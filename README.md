This repository is for the content of the talk @tad-lispy is going to deliver at [Rust Developers Amsterdam meetup on Tuesday, 2022-05-24 at 18:30](https://www.meetup.com/rust-amsterdam-group/events/285291653/).

# Clarity for the Nerd

Modeling business domain using the framework of choices and combinations and the Rust programming language.

✍️ [the essay](https://software-garden.gitlab.io/rust-business-modeling/)

👨‍🏫 [the slides](https://software-garden.gitlab.io/rust-business-modeling/slides.html)

💬 [the script](./script.md)

The talk is suppose to be 25 minutes long and is based on the [Domain Modeling Made Functional](https://www.youtube.com/watch?v=2JB1_e5wZmU) by [Scott Wlaschin](https://scottwlaschin.com/).


# About Software Garden

[Software Garden](https://software.garden/) is an informal community of software professionals based in western Europe. Our members offer consulting, coaching and similar services in the area of software development. We also offer paid as well as free workshops for developers, designers, product and business people to help participants collaborate better. We are all about happiness and efficiency at work.

